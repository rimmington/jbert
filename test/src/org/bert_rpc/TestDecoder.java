package org.bert_rpc;

import org.hamcrest.core.IsInstanceOf;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Tests decoding BERT terms to Java objects.
 */
public class TestDecoder {
    private static final Date date = new Date(1254976067000L);
    private static final HashMap<Atom, Atom> map = new HashMap<Atom, Atom>();
    static {
        TestDecoder.map.put(new Atom("name"), new Atom("symbol"));
    }

    public static final Tuple complexTuple = new Tuple(new Atom("user"),
            TestDecoder.map, Arrays.asList(9.9), TestDecoder.date, null, true,
            false, new Atom("true"), new Atom("false"));
    public static final byte[] complexBert = new byte[] { -125, 104, 9, 100, 0,
            4, 117, 115, 101, 114, 104, 3, 100, 0, 4, 98, 101, 114, 116, 100,
            0, 4, 100, 105, 99, 116, 108, 0, 0, 0, 1, 104, 2, 100, 0, 4, 110,
            97, 109, 101, 100, 0, 6, 115, 121, 109, 98, 111, 108, 106, 108, 0,
            0, 0, 1, 99, 57, 46, 57, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48,
            48, 48, 48, 48, 101, 43, 48, 48, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 106,
            104, 5, 100, 0, 4, 98, 101, 114, 116, 100, 0, 4, 116, 105, 109,
            101, 98, 0, 0, 4, -26, 98, 0, 14, -28, -61, 97, 0, 104, 2, 100, 0,
            4, 98, 101, 114, 116, 100, 0, 3, 110, 105, 108, 104, 2, 100, 0, 4,
            98, 101, 114, 116, 100, 0, 4, 116, 114, 117, 101, 104, 2, 100, 0,
            4, 98, 101, 114, 116, 100, 0, 5, 102, 97, 108, 115, 101, 100, 0, 4,
            116, 114, 117, 101, 100, 0, 5, 102, 97, 108, 115, 101 };

    @Test
    public void testDecodeIntegers() throws Exception {
        Assert.assertEquals("decodes small ints",
                42,
                Bert.decode(new byte[] { -125, 97, 42 }));
        Assert.assertEquals("decodes small ints >= 128",
                128,
                Bert.decode(new byte[] { -125, 97, (byte)128 }));
        Assert.assertEquals("decodes large ints",
                4200,
                Bert.decode(new byte[] { -125, 98, 0, 0, 16, 104 }));
    }

    @Test
    public void testDecodeTuples() throws Exception {
        Assert.assertEquals("decodes tuples",
                new Tuple(new Atom("foo"), new Atom("bar")),
                Bert.decode(new byte[] { -125, 104, 2, 100, 0, 3, 102, 111,
                        111, 100, 0, 3, 98, 97, 114 }));
    }

    @Test
    public void testDecodeStrings() throws Exception {
        Object bin = Bert.decode(new byte[]{ -125, 109, 0, 0, 0, 5, 104, 101,
                108, 108, 111});
        Assert.assertThat("recognises binary",
                bin,
                new IsInstanceOf(byte[].class));
        Assert.assertArrayEquals("decodes binary",
                new byte[]{104, 101, 108, 108, 111},
                (byte[]) bin);

        Assert.assertEquals("decodes strings",
                "hello",
                new String((byte[]) Bert.decode(new byte[] { -125, 109, 0, 0,
                        0, 5, 104, 101, 108, 108, 111 })));
    }

    @Test
    public void testDecodeFloats() throws Exception {
        Assert.assertEquals("decodes floats",
                5.5,
                Bert.decode(new byte[] { -125, 99, 53, 46, 53, 48, 48, 48, 48,
                        48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 101, 43, 48,
                        48, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
    }

    @Test
    public void testDecodeLists() throws Exception {
        Assert.assertEquals("decodes lists",
                Arrays.asList(new Atom("a"), 1, 5),
                Bert.decode(new byte[] { -125, 108, 0, 0, 0, 3, 100, 0, 1, 97,
                        97, 1, 97, 5, 106 }));
        Assert.assertEquals("decodes a list followed by another value",
                new Tuple(Arrays.asList(1), new Atom("foo")),
                Bert.decode(new byte[] { -125, 104, 2, 108, 0, 0, 0, 1, 97, 1,
                        106, 100, 0, 3, 102, 111, 111 }));
    }

    @Test
    public void testDecodeDicts() throws Exception {
        Map<Object, Object> expected = new HashMap<Object, Object>();
        expected.put(new Atom("foo"), 1);
        Assert.assertEquals("decodes dicts",
                expected,
                Bert.decode(new byte[] { -125, 104, 3, 100, 0, 4, 98, 101, 114,
                        116, 100, 0, 4, 100, 105, 99, 116, 108, 0, 0, 0, 1,
                        104, 2, 100, 0, 3, 102, 111, 111, 97, 1, 106 }));
    }

    @Test
    public void testDecodeTimes() throws Exception {
        Assert.assertEquals("decodes time",
                new Date(1254976067000L),
                Bert.decode(new byte[] { -125, 104, 5, 100, 0, 4, 98, 101, 114,
                        116, 100, 0, 4, 116, 105, 109, 101, 98, 0, 0, 4, -26,
                        98, 0, 14, -28, -61, 97, 0 }));
    }

    @Test
    public void testDecodeConstants() throws Exception {
        Assert.assertSame("decodes nil",
                null,
                Bert.decode(new byte[]{-125, 104, 2, 100, 0, 4, 98, 101, 114,
                        116, 100, 0, 3, 110, 105, 108}));
        Assert.assertSame("decodes true",
                true,
                Bert.decode(new byte[] { -125, 104, 2, 100, 0, 4, 98, 101, 114,
                        116, 100, 0, 4, 116, 114, 117, 101 }));
        Assert.assertSame("decodes false",
                false,
                Bert.decode(new byte[] { -125, 104, 2, 100, 0, 4, 98, 101, 114,
                        116, 100, 0, 5, 102, 97, 108, 115, 101 }));
    }

    @Test
    public void testDecodeAtoms() throws Exception {
        Assert.assertEquals("decodes atoms",
                new Atom("foo"),
                Bert.decode(new byte[] { -125, 100, 0, 3, 102, 111, 111 }));
    }

    @Test
    public void testDecodeComplexTuple() throws Exception {
        Assert.assertEquals("decodes a complex tuple",
                TestDecoder.complexTuple,
                Bert.decode(TestDecoder.complexBert));
    }
}
