package org.bert_rpc;

import org.junit.Assert;
import org.junit.Test;

import java.util.regex.Pattern;

public class TestEncodeDecode {
    @Test
    public void testRegexen() throws Exception {
        Pattern expected = Pattern.compile("(foo|spam)|(bar|eggs)",
                Pattern.MULTILINE | Pattern.UNIX_LINES);
        Pattern result = (Pattern)Bert.decode(Bert.encode(expected));
        Assert.assertEquals("encodes & decodes regex patterns",
                expected.pattern(),
                result.pattern());
        Assert.assertEquals("encodes & decodes regex flags",
                expected.flags(),
                result.flags());
    }
}
