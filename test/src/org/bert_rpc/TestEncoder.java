package org.bert_rpc;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.regex.Pattern;

public class TestEncoder {
    @Test
    public void testEncodesStrings() throws Exception {
        Assert.assertArrayEquals("encodes strings",
                new byte[] { -125, 109, 0, 0, 0, 5, 104, 101, 108, 108, 111 },
                Bert.encode("hello".getBytes()));
    }

    @Test
    public void testEncodesTuples() throws Exception {
        Assert.assertArrayEquals("encodes tuples",
                new byte[] { -125, 104, 2, 100, 0, 3, 102, 111, 111, 100, 0, 3,
                        98, 97, 114 },
                Bert.encode(new Tuple(new Atom("foo"), new Atom("bar"))));
    }

    @Test(expected = EncodingException.class)
    public void testDisallowsBertAtomFirst() throws Exception {
        Bert.encode(new Tuple(new Atom("bert"), new Atom("foo")));
    }

    @Test
    public void testEncodesFloats() throws Exception {
        Assert.assertArrayEquals("encodes floats",
                new byte[] { -125, 99, 53, 46, 53, 48, 48, 48, 48, 48, 48, 48,
                        48, 48, 48, 48, 48, 48, 48, 101, 43, 48, 48, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0 },
                Bert.encode(5.5));
    }

    @Test
    public void testEncodesDates() throws Exception {
        Assert.assertArrayEquals("encodes dates",
                new byte[] { -125, 104, 5, 100, 0, 4, 98, 101, 114, 116, 100,
                        0, 4, 116, 105, 109, 101, 98, 0, 0, 4, -46, 98, 0, 8,
                        -86, 82, 97, 0 },
                Bert.encode(new Date(1234567890000L)));
    }

    @Test
    public void testEncodesRegexen() throws Exception {
        Assert.assertArrayEquals("encodes regexen",
                new byte[] { -125, 104, 4, 100, 0, 4, 98, 101, 114, 116, 100,
                        0, 5, 114, 101, 103, 101, 120, 109, 0, 0, 0, 21, 40,
                        102, 111, 111, 124, 115, 112, 97, 109, 41, 124, 40, 98,
                        97, 114, 124, 101, 103, 103, 115, 41, 106 },
                Bert.encode(Pattern.compile("(foo|spam)|(bar|eggs)")));
    }

    @Test(expected = EncodingException.class)
    public void testUnsupportedRegexFlags() throws Exception {
        Pattern invalid = Pattern.compile("(foo|spam)|(bar|eggs)",
                Pattern.LITERAL);
        Bert.encode(invalid);
    }

    @Test
    public void testEncodesIntegers() throws Exception {
        Assert.assertArrayEquals("encodes integers < 128",
                new byte[] { -125, 97, 5 },
                Bert.encode(5));
        Assert.assertArrayEquals("encodes integers >= 128",
                new byte[] { -125, 97, (byte)128 },
                Bert.encode(128));
    }

    @Test
    public void testEncodesComplexTuple() throws Exception {
        Assert.assertArrayEquals("encodes a complex tuple",
                TestDecoder.complexBert,
                Bert.encode(TestDecoder.complexTuple));
    }
}
