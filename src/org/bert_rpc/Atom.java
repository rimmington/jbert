package org.bert_rpc;

/**
 * A BERT atom: a literal, a constant with name.
 */
public class Atom {
    private final String name;

    /**
     * Creates an atom with the specified name.
     *
     * @param name The name of the new atom.
     */
    public Atom(String name) {
        this.name = name;
    }

    /**
     * Gets the name of this atom.
     *
     * @return The name of this atom.
     */
    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return "'" + this.name;
    }

    @Override
    public boolean equals(Object other) {
        return other != null &&
                (other instanceof Atom) &&
                this.equals((Atom)other);
    }

    /**
     * Indicates whether some other atom has the same name as this one.
     *
     * @param other The atom to compare this one to.
     * @return true if other and this atom have the same name, false otherwise.
     */
    public boolean equals(Atom other) {
        return other != null && this.name.equals(other.name);
    }

    @Override
    public int hashCode() {
        return 524287 * this.name.hashCode();
    }
}
