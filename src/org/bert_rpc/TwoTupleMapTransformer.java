package org.bert_rpc;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.Map;

/**
 * Collection that produces Tuple(key, value) pairs from a map. Only a list
 * because that's what Encoder#encodeUnknownType checks for; calling get(int)
 * will throw an UnsupportedOperationException.
 *
 * @param <K> The map key type.
 * @param <V> The map value type.
 */
class TwoTupleMapTransformer<K, V> extends AbstractList<Tuple> {
    private final Map<K, V> map;

    public TwoTupleMapTransformer(Map<K, V> map) {
        this.map = map;
    }

    @Override
    public int size() {
        return this.map.size();
    }

    @Override
    public Iterator<Tuple> iterator() {
        return new TwoTupleMapIterator<K, V>(this.map.entrySet().iterator());
    }

    @Override
    public Tuple get(int index) {
        throw new UnsupportedOperationException();
    }
}
