package org.bert_rpc;

/**
 * Signals that the provided input cannot be decoded according to the BERT
 * spec to an interoperable object.
 */
public class DecodingException extends Exception {
    public DecodingException(String message) {
        super(message);
    }
}
