/**
 * An implementation of the BERT binary data interchange format for Java.
 *
 * Supports encoding and decoding of the following types:
 * <ul>
 *     <li>{@link org.bert_rpc.Atom}</li>
 *     <li>Boolean</li>
 *     <li>byte[]</li>
 *     <li>java.util.Date</li>
 *     <li>Double</li>
 *     <li>Float</li>
 *     <li>Integer</li>
 *     <li>java.util.List</li>
 *     <li>java.util.Map</li>
 *     <li>java.util.regex.Pattern</li>
 *     <li>{@link org.bert_rpc.Tuple}</li>
 * </ul>
 *
 * Note that regex Pattern encoding and decoding is a best-effort attempt at
 * conforming to the spec. Some flags are not supported because they have no
 * Java equivalent; thus, users must restrict themselves to some common subset
 * of flags. In addition, the pattern component is assumed to be encoded as
 * UTF-8, since Java does not accept binary patterns.
 */

package org.bert_rpc;
