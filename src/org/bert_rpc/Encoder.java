package org.bert_rpc;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Encodes Java objects into BERT terms.
 */
public class Encoder {
    private final DataOutputStream outputStream;

    /**
     * Creates an Encoder that writes to the provided output stream.
     * @param outputStream The output stream to write BERT terms from.
     */
    public Encoder(OutputStream outputStream) {
        this.outputStream = new DataOutputStream(outputStream);
    }

    /**
     * Encodes an object with any type on the BERT supported types list.
     *
     * @param object The object to encode.
     * @throws IOException Signals that an I/O exception of some sort has
     * occurred.
     * @throws EncodingException Signals that the provided object cannot be
     * encoded according to the BERT spec to an interoperable output.
     */
    public void encodeUnknownType(Object object)
            throws IOException, EncodingException {
        this.outputStream.writeByte(Constants.MAGIC);
        this.writeUnknownType(object);
        this.outputStream.flush();
    }

    @SuppressWarnings("unchecked") // for Map to Map<K, V> cast
    private void writeUnknownType(Object value)
            throws EncodingException, IOException {
        if (value == null) {
            this.writeNull();
        } else if (value instanceof Tuple) {
            this.writeTuple((Tuple) value);
        } else if (value instanceof Atom) {
            this.writeAtom((Atom) value);
        } else if (value instanceof Map) {
            this.writeMap((Map) value);
        } else if (value instanceof Date) {
            this.writeDate((Date) value);
        } else if (value instanceof Integer) {
            this.writeInt((Integer) value);
        } else if (value instanceof Double) {
            this.writeDouble((Double) value);
        } else if (value instanceof Float) {
            this.writeFloat((Float) value);
        } else if (value instanceof Boolean) {
            this.writeBoolean((Boolean) value);
        } else if (value instanceof byte[]) {
            this.writeBinary((byte[]) value);
        } else if (value instanceof List) {
            this.writeList((List) value);
        } else if (value instanceof Pattern) {
            this.writeRegex((Pattern) value);
        } else {
            throw new EncodingException("cannot encode objects of type "
                + value.getClass());
        }
    }

    /**
     * Encodes a tuple containing any types on the BERT supported types list.
     *
     * @param tuple The tuple to encode.
     * @throws IOException Signals that an I/O exception of some sort has
     * occurred.
     * @throws EncodingException Signals that the provided object cannot be
     * encoded according to the BERT spec to an interoperable output.
     */
    public void encode(Tuple tuple) throws IOException, EncodingException {
        this.outputStream.writeByte(Constants.MAGIC);
        if (tuple == null) this.writeNull(); else this.writeTuple(tuple);
        this.outputStream.flush();
    }

    /**
     * Writes a tuple that is user-provided and cannot be a complex type.
     *
     * @param tuple The tuple to write.
     * @throws IOException Signals that an I/O exception of some sort has
     * occurred.
     * @throws EncodingException Signals that the provided object cannot be
     * encoded according to the BERT spec to an interoperable output.
     */
    private void writeTuple(Tuple tuple)
            throws IOException, EncodingException {
        if (tuple.size() > 0 && Constants.$bert.equals(tuple.get(0))) {
            throw new EncodingException(
                    "tuple cannot have atom 'bert in position 0.");
        } else {
            this.writeComplexTuple(tuple);
        }
    }

    /**
     * Writes a tuple that may be a complex type.
     *
     * @param tuple The tuple to write.
     * @throws IOException Signals that an I/O exception of some sort has
     * occurred.
     * @throws EncodingException Signals that the provided object cannot be
     * encoded according to the BERT spec to an interoperable output.
     */
    private void writeComplexTuple(Tuple tuple)
            throws IOException, EncodingException {
        if (tuple.size() < 256) {
            this.outputStream.write(Constants.SMALL_TUPLE);
            this.outputStream.writeByte(tuple.size());
        } else {
            this.outputStream.write(Constants.LARGE_TUPLE);
            this.outputStream.writeInt(tuple.size());
        }

        for (Object element : tuple) {
            this.writeUnknownType(element);
        }
    }

    public void encode(Atom atom) throws IOException {
        this.outputStream.writeByte(Constants.MAGIC);
        if (atom == null) this.writeNull(); else this.writeAtom(atom);
        this.outputStream.flush();
    }

    private void writeAtom(Atom atom) throws IOException {
        this.outputStream.writeByte(Constants.ATOM);
        this.outputStream.writeShort(atom.getName().length());
        this.outputStream.writeBytes(atom.getName());
    }

    /**
     * Encodes a map containing any types on the BERT supported types list.
     *
     * @param map The map to encode.
     * @throws IOException Signals that an I/O exception of some sort has
     * occurred.
     * @throws EncodingException Signals that the provided object cannot be
     * encoded according to the BERT spec to an interoperable output.
     */
    public <K, V> void encode(Map<K, V> map)
            throws IOException, EncodingException {
        this.outputStream.writeByte(Constants.MAGIC);
        if (map == null) this.writeNull(); else this.writeMap(map);
        this.outputStream.flush();
    }

    private <K, V> void writeMap(Map<K, V> map)
            throws IOException, EncodingException {
        this.writeComplexTuple(new Tuple(Constants.$bert, Constants.$dict,
                new TwoTupleMapTransformer<K, V>(map)));
    }

    /**
     * Encodes a date.
     *
     * @param date The date to encode.
     * @throws IOException Signals that an I/O exception of some sort has
     * occurred.
     */
    public void encode(Date date) throws IOException {
        this.outputStream.writeByte(Constants.MAGIC);
        if (date == null) this.writeNull(); else this.writeDate(date);
        this.outputStream.flush();
    }

    private void writeDate(Date date) throws IOException {
        long millis = date.getTime();

        try {
            this.writeComplexTuple(new Tuple(Constants.$bert, Constants.$time,
                    (int) Math.floor(millis / 1e9),
                    (int) Math.floor((millis % 1e9) / 1e3),
                    (int) Math.floor(millis % 1e3)));
        } catch (EncodingException e) {
            throw new RuntimeException(
                    "somehow failed to encode date. this is a bug in jbert.",
                    e);
        }
    }

    /**
     * Encodes an integer value that could be null.
     *
     * @param value The value to encode.
     * @throws IOException Signals that an I/O exception of some sort has
     * occurred.
     */
    public void encode(Integer value) throws IOException {
        if (value == null) this.encodeNull(); else this.encode((int)value);
    }

    /**
     * Encodes an integer value.
     *
     * @param value The value to encode.
     * @throws IOException Signals that an I/O exception of some sort has
     * occurred.
     */
    public void encode(int value) throws IOException {
        this.outputStream.writeByte(Constants.MAGIC);
        this.writeInt(value);
        this.outputStream.flush();
    }

    private void writeInt(int value) throws IOException {
        if (value >= 0 && value < 256) {
            this.outputStream.writeByte(Constants.SMALL_INT);
            this.outputStream.writeByte(value);
        } else {
            this.outputStream.writeByte(Constants.INT);
            this.outputStream.writeInt(value);
        }
    }

    /**
     * Encodes a double-precision floating point value that could be null.
     *
     * @param value The value to encode.
     * @throws IOException Signals that an I/O exception of some sort has
     * occurred.
     */
    public void encode(Double value) throws IOException {
        if (value == null) this.encodeNull(); else this.encode((double)value);
    }

    /**
     * Encodes a double-precision floating point value.
     *
     * @param value The value to encode.
     * @throws IOException Signals that an I/O exception of some sort has
     * occurred.
     */
    public void encode(double value) throws IOException {
        this.outputStream.writeByte(Constants.MAGIC);
        this.writeDouble(value);
        this.outputStream.flush();
    }

    private void writeDouble(double value) throws IOException {
        byte[] bytes = String.format("%15.15e", value).getBytes();
        this.outputStream.writeByte(Constants.FLOAT);
        this.outputStream.write(Arrays.copyOf(bytes, Constants.FLOAT_LENGTH));
    }

    /**
     * Encodes a floating point value that could be null.
     *
     * @param value The value to encode.
     * @throws IOException Signals that an I/O exception of some sort has
     * occurred.
     */
    public void encode(Float value) throws IOException {
        if (value == null) this.encodeNull(); else this.encode((float)value);
    }

    /**
     * Encodes a floating point value.
     *
     * @param value The value to encode.
     * @throws IOException Signals that an I/O exception of some sort has
     * occurred.
     */
    public void encode(float value) throws IOException {
        this.outputStream.writeByte(Constants.MAGIC);
        this.writeFloat(value);
        this.outputStream.flush();
    }

    private void writeFloat(float value) throws IOException {
        this.writeDouble((double) value);
    }

    /**
     * Encodes a boolean value that could be null.
     *
     * @param value The value to encode.
     * @throws IOException Signals that an I/O exception of some sort has
     * occurred.
     */
    public void encode(Boolean value) throws IOException {
        if (value == null) this.encodeNull(); else this.encode((boolean)value);
    }

    /**
     * Encodes a boolean value.
     *
     * @param value The value to encode.
     * @throws IOException Signals that an I/O exception of some sort has
     * occurred.
     */
    public void encode(boolean value) throws IOException {
        this.outputStream.writeByte(Constants.MAGIC);
        this.writeBoolean(value);
        this.outputStream.flush();
    }

    private void writeBoolean(boolean bool)
            throws IOException {
        Atom representation;
        if (bool) {
            representation = Constants.$true;
        } else {
            representation = Constants.$false;
        }

        try {
            this.writeComplexTuple(new Tuple(Constants.$bert, representation));
        } catch (EncodingException e) {
            throw new RuntimeException("somehow failed to encode boolean. " +
                    "this is a bug in jbert.", e);
        }
    }

    /**
     * Encodes a null value.
     *
     * @throws IOException Signals that an I/O exception of some sort has
     * occurred.
     */
    public void encodeNull() throws IOException {
        this.outputStream.writeByte(Constants.MAGIC);
        this.writeNull();
        this.outputStream.flush();
    }

    private void writeNull() throws IOException {
        try {
            this.writeComplexTuple(new Tuple(Constants.$bert, Constants.$nil));
        } catch (EncodingException e) {
            throw new RuntimeException(
                    "somehow failed to encode null. this is a bug in jbert.",
                    e);
        }
    }

    /**
     * Encodes a binary sequence.
     *
     * @param bytes The bytes to encode.
     * @throws IOException Signals that an I/O exception of some sort has
     * occurred.
     */
    public void encode(byte[] bytes) throws IOException {
        this.outputStream.writeByte(Constants.MAGIC);
        if (bytes == null) this.writeNull(); else this.writeBinary(bytes);
        this.outputStream.flush();
    }

    private void writeBinary(byte[] bytes) throws IOException {
        this.outputStream.writeByte(Constants.BIN);
        this.outputStream.writeInt(bytes.length);
        this.outputStream.write(bytes);
    }

    /**
     * Encodes a list containing any types on the BERT supported types list.
     *
     * @param list The list to encode.
     * @throws IOException Signals that an I/O exception of some sort has
     * occurred.
     * @throws EncodingException Signals that the provided object cannot be
     * encoded according to the BERT spec to an interoperable output.
     */
    public void encode(List list) throws IOException, EncodingException {
        this.outputStream.writeByte(Constants.MAGIC);
        if (list == null) this.writeNull(); else this.writeList(list);
        this.outputStream.flush();
    }

    private void writeList(List list) throws IOException, EncodingException {
        if (list.size() == 0) {
            this.outputStream.writeByte(Constants.NIL);
        } else {
            this.outputStream.writeByte(Constants.LIST);
            this.outputStream.writeInt(list.size());

            for (Object element : list) {
                this.writeUnknownType(element);
            }

            this.outputStream.writeByte(Constants.NIL);
        }
    }

    /**
     * Encodes a regular expression.
     *
     * @param regex The regular expression to encode.
     * @throws IOException Signals that an I/O exception of some sort has
     * occurred.
     * @throws EncodingException Signals that the regular expression contains
     * unsupported flags.
     */
    public void encode(Pattern regex) throws IOException, EncodingException {
        this.outputStream.writeByte(Constants.MAGIC);
        if (regex == null) this.writeNull(); else this.writeRegex(regex);
        this.outputStream.flush();
    }

    private void writeRegex(Pattern regex)
            throws IOException, EncodingException {
        List<Object> flags = new ArrayList<Object>();
        int runningMask = regex.flags();
        for (Map.Entry<Object, Integer> p : Constants.REGEX_FLAGS.entrySet()) {
            if ((runningMask & p.getValue()) != 0) {
                runningMask = runningMask ^ p.getValue();
                flags.add(p.getKey());
            }
        }

        if (runningMask != 0) {
            throw new EncodingException("regex contains unsupported flags.");
        }

        try {
            this.writeComplexTuple(new Tuple(Constants.$bert, Constants.$regex,
                    regex.pattern().getBytes("UTF8"),
                    flags));
        } catch (EncodingException e) {
            throw new RuntimeException("somehow failed to encode valid regex. "
                    + "this is a bug in jbert.", e);
        }
    }
}
