package org.bert_rpc;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Decodes BERT terms into Java objects.
 */
public class Decoder {
    private final DataInputStream inputStream;

    /**
     * Creates a Decoder that reads from the provided input stream.
     *
     * @param inputStream The input stream to read BERT terms from.
     */
    public Decoder(InputStream inputStream) {
        this.inputStream = new DataInputStream(inputStream);
    }

    /**
     * Reads a single object from the input stream.
     *
     * @return A decoded object with a type on the supported type list.
     * @throws DecodingException Signals that the provided input cannot be
     * decoded according to the BERT spec to an interoperable object.
     * @throws IOException Signals that an I/O exception of some sort has
     * occurred.
     *
     * @see org.bert_rpc
     */
    public Object decode() throws DecodingException, IOException {
        int v = this.inputStream.read();
        if (v != Constants.MAGIC) {
            throw new DecodingException("invalid format");
        } else {
            return this.read();
        }
    }

    private Object read() throws DecodingException, IOException {
        Object simple = this.readSimple();
        if (simple instanceof Tuple) {
            Tuple tuple = (Tuple)simple;
            if (tuple.size() > 0 &&
                    Constants.$bert.equals(tuple.get(0))) {
                return this.extractComplex(tuple);
            }
        }

        return simple;
    }

    private Object extractComplex(Tuple tuple) throws DecodingException {
        if (tuple.size() <= 1) {
            throw new DecodingException(
                    "complex type tuple must have length >= 2");
        } else if (!(tuple.get(1) instanceof Atom)) {
            throw new DecodingException(
                    "second element of complex type tuple must be atom");
        }

        Atom type = (Atom)tuple.get(1);
        if (type.equals(Constants.$dict)) {
            return this.extractDict(tuple);
        } else if (type.equals(Constants.$time)) {
            return this.extractTime(tuple);
        } else if (type.equals(Constants.$regex)) {
            return this.extractRegex(tuple);
        } else if (type.equals(Constants.$nil)) {
            return null;
        } else if (type.equals(Constants.$true)) {
            return true;
        } else if (type.equals(Constants.$false)) {
            return false;
        } else {
            throw new DecodingException("unknown complex type " + type);
        }
    }

    @SuppressWarnings("unchecked") // for List<Tuple> cast
    private Map<Object, Object> extractDict(Tuple tuple)
            throws DecodingException {
        if (tuple.size() != 3) {
            throw new DecodingException("dict type must be 3-tuple");
        }

        List<Tuple> pairs;
        try {
            pairs = (List<Tuple>) tuple.get(2);
        } catch (ClassCastException e) {
            throw new DecodingException(
                    "third element of dict type tuple must be list of " +
                            "2-tuples");
        }

        Map<Object, Object> dict = new HashMap<Object, Object>(pairs.size());
        for (Tuple pair : pairs) {
            if (pair.size() != 2) {
                throw new DecodingException(
                        "dict tuple pairs must be 2-tuples");
            }

            dict.put(pair.get(0), pair.get(1));
        }

        return dict;
    }

    private Date extractTime(Tuple tuple) throws DecodingException {
        if (tuple.size() != 5) {
            throw new DecodingException("time type must be 5-tuple");
        }

        double stamp;
        try {
            stamp = (Integer)tuple.get(2) * 1e9 +
                    (Integer)tuple.get(3) * 1e3 +
                    (Integer)tuple.get(4);
        } catch (ClassCastException e) {
            throw new DecodingException("time type contain 3 integers");
        }

        return new Date((long) stamp);
    }

    // This is a best-effort attempt at conforming to the spec. Some flags are
    // not supported because they have no Java equivalent; thus, users must
    // restrict themselves to some common subset of flags. In addition, the
    // pattern component is assumed to be encoded as UTF-8, since Java does
    // not accept binary patterns.
    private Pattern extractRegex(Tuple tuple) throws DecodingException {
        if (tuple.size() != 4) {
            throw new DecodingException("regex type must be 4-tuple");
        } else if (!(tuple.get(2) instanceof byte[])) {
            throw new DecodingException(
                    "regex type must have binary component at index 2");
        } else if (!(tuple.get(3) instanceof List)) {
            throw new DecodingException(
                    "regex type must have list at index 3");
        }

        List flags = (List)tuple.get(3);
        int mask = 0;
        for (Object flag : flags) {
            Integer maskComponent = Constants.REGEX_FLAGS.get(flag);
            if (maskComponent != null) {
                mask = (mask | (int)maskComponent);
            } else {
                throw new DecodingException("unsupported regex flag "
                        + flag);
            }
        }

        return Pattern.compile(
                new String((byte[])tuple.get(2), Charset.forName("UTF8")),
                mask);
    }

    private Object readSimple() throws IOException, DecodingException {
        int value = this.inputStream.read();
        switch (value) {
            case Constants.BIN:
                return this.readBin();
            case Constants.SMALL_TUPLE:
                return this.readSmallTuple();
            case Constants.NIL:
                return Collections.emptyList();
            case Constants.LIST:
                return this.readList();
            case Constants.SMALL_INT:
                return this.readSmallInt();
            case Constants.INT:
                return this.readInt();
            case Constants.ATOM:
                return this.readAtom();
            case Constants.FLOAT:
                return this.readDouble();
            default:
                throw new DecodingException("unknown type: " + value);
        }
    }

    private byte[] readBytes(int length) throws IOException {
        byte[] array = new byte[length];
        this.inputStream.readFully(array);
        return array;
    }

    private byte[] readBin() throws IOException {
        int length = this.readInt();
        return this.readBytes(length);
    }

    private Tuple readSmallTuple() throws IOException, DecodingException {
        int length = this.inputStream.readByte();
        Object[] contents = new Object[length];
        for (int i = 0; i < length; i++) {
            contents[i] = this.read();
        }

        return new Tuple(contents);
    }

    private List<Object> readList() throws IOException, DecodingException {
        int length = this.readInt();
        ArrayList<Object> list = new ArrayList<Object>(length);
        for (int i = 0; i < length; i++) {
            list.add(this.read());
        }

        if (this.inputStream.readByte() == Constants.NIL) {
            return list;
        } else {
            throw new DecodingException("cannot decode improper list");
        }
    }

    private int readSmallInt() throws IOException {
        return this.inputStream.readUnsignedByte();
    }

    private int readInt() throws IOException {
        return this.inputStream.readInt();
    }

    private Atom readAtom() throws IOException {
        int length = this.inputStream.readShort();
        return new Atom(new String(this.readBytes(length)));
    }

    private double readDouble() throws IOException {
        return new Double(new String(this.readBytes(Constants.FLOAT_LENGTH)));
    }
}
