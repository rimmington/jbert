package org.bert_rpc;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * BERT static convenience methods.
 */
public class Bert {
    private Bert() {}

    /**
     * Reads a single object from the provided byte array.
     *
     * @param bytes A byte array containing a BERT term.
     * @return A decoded object with a type on the supported type list.
     * @throws DecodingException Signals that the provided input cannot be
     * decoded according to the BERT spec to an interoperable object.
     *
     * @see org.bert_rpc
     */
    public static Object decode(final byte[] bytes) throws DecodingException {
        try {
            return new Decoder(new ByteArrayInputStream(bytes)).decode();
        } catch (IOException e) {
            throw new RuntimeException("Reading from array failed somehow: "
                    + e.getMessage(), e);
        }
    }

    /**
     * Encodes an object with any type on the BERT supported types list onto
     * a byte array.
     *
     * @param object The object to encode.
     * @throws EncodingException Signals that the provided object cannot be
     * encoded according to the BERT spec to an interoperable output.
     */
    public static byte[] encode(Object object) throws EncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            new Encoder(out).encodeUnknownType(object);
            out.flush();
        } catch (IOException e) {
            throw new RuntimeException("Writing to array failed somehow: "
                    + e.getMessage(), e);
        }
        return out.toByteArray();
    }
}
