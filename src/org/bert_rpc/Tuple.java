package org.bert_rpc;

import java.util.AbstractList;
import java.util.Arrays;

/**
 * A BERT tuple: a compound data type with a fixed number of terms.
 */
public class Tuple extends AbstractList {
    private final Object[] contents;

    /**
     * Creates a new empty tuple.
     */
    public Tuple() {
        this(new Object[0]);
    }

    /**
     * Creates a new tuple with the provided contents.
     *
     * @param contents The contents of the new tuple.
     */
    public Tuple(Object... contents) {
        this.contents = contents;
    }

    @Override
    public String toString() {
        String cs = Arrays.toString(this.contents);
        return "Tuple(" + cs.substring(1, cs.length() - 1) + ")";
    }

    /**
     * Indicates whether some other tuple has contents equal to this one's.
     *
     * @param other The tuple to compare this one to.
     * @return true if the contents of this tuple and the other are equal,
     * false otherwise.
     */
    public boolean equals(Tuple other) {
        return Arrays.equals(this.contents, other.contents);
    }

    @Override
    public boolean equals(Object other) {
        return other != null &&
                (other instanceof Tuple) &&
                this.equals((Tuple)other);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(this.contents);
    }

    @Override
    public int size() {
        return this.contents.length;
    }

    @Override
    public Object get(int index) {
        return this.contents[index];
    }
}
