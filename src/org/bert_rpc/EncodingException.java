package org.bert_rpc;

/**
 * Signals that the provided object cannot be encoded according to the BERT
 * spec to an interoperable output.
 */
public class EncodingException extends Exception {
    public EncodingException(String message) {
        super(message);
    }
}
