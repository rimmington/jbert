package org.bert_rpc;

import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * Iterator that produces Tuple(key, value) pairs from a Map.Entry iterator.
 *
 * @param <K> The map key type.
 * @param <V> The map value type.
 */
class TwoTupleMapIterator<K, V> implements Iterator<Tuple> {
    private final Iterator<Map.Entry<K, V>> mapIterator;

    public TwoTupleMapIterator(Iterator<Map.Entry<K, V>> mapIterator) {
        this.mapIterator = mapIterator;
    }

    @Override
    public boolean hasNext() {
        return this.mapIterator.hasNext();
    }

    @Override
    public Tuple next() {
        if (this.hasNext()) {
            Map.Entry nxt = this.mapIterator.next();
            return new Tuple(nxt.getKey(), nxt.getValue());
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
