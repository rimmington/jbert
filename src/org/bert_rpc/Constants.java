package org.bert_rpc;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * BERT magic values.
 */
class Constants {
    public static final byte SMALL_INT = 97;
    public static final byte INT = 98;
//    public static final byte SMALL_BIGNUM = 110;
//    public static final byte LARGE_BIGNUM = 111;
    public static final byte FLOAT = 99;
    public static final byte ATOM = 100;
    public static final byte SMALL_TUPLE = 104;
    public static final byte LARGE_TUPLE = 105;
    public static final byte NIL = 106;
//    public static final byte STRING = 107;
    public static final byte LIST = 108;
    public static final byte BIN = 109;
//    public static final byte FUN = 117;
//    public static final byte NEW_FUN = 112;
    public static final int MAGIC = 131;
//    public static final int MAX_INT =  (1 << 27) -1;
//    public static final int MIN_INT = -(1 << 27);

    public static final byte FLOAT_LENGTH = 31;

    public static final Atom $bert = new Atom("bert");
    public static final Atom $dict = new Atom("dict");
    public static final Atom $time = new Atom("time");
    public static final Atom $regex = new Atom("regex");
    public static final Atom $nil = new Atom("nil");
    public static final Atom $true = new Atom("true");
    public static final Atom $false = new Atom("false");

    public static final Atom $caseless = new Atom("caseless");
    public static final Atom $dotall = new Atom("dotall");
    public static final Atom $extended = new Atom("extended");
    public static final Atom $multiline = new Atom("multiline");
    public static final Atom $newline = new Atom("newline");
    public static final Atom $lf = new Atom("lf");

    public static final Map<Object, Integer> REGEX_FLAGS =
            new HashMap<Object, Integer>();
    static {
        Constants.REGEX_FLAGS.put(Constants.$caseless,
                Pattern.CASE_INSENSITIVE);
        Constants.REGEX_FLAGS.put(Constants.$dotall, Pattern.DOTALL);
        Constants.REGEX_FLAGS.put(Constants.$extended, Pattern.COMMENTS);
        Constants.REGEX_FLAGS.put(Constants.$multiline, Pattern.MULTILINE);
        Constants.REGEX_FLAGS.put(new Tuple(Constants.$newline, Constants.$lf),
                Pattern.UNIX_LINES);
    }
}
